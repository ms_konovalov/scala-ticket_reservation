package ms.konovalov.ticketreservation

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{HttpEntity, MediaTypes}
import spray.json.DefaultJsonProtocol

/** Compound key for movie session
  *
  * @param imdbId   IMDB identifier
  * @param screenId session identifier
  */
case class MovieSessionId(imdbId: String, screenId: String)

/** Structure to store info about seats
  *
  * @param total     total seats
  * @param available available seats
  */
case class Seats(total: Int, available: Int)

/** Request to add movie session into system
  *
  * @param imdbId         IMDB identifier
  * @param screenId       session identifier
  * @param availableSeats available seats
  */
case class RegisterMovieRequest(imdbId: String, screenId: String, availableSeats: Int)

/** Request to reserve 1 seat for movie session
  *
  * @param imdbId   IMDB identifier
  * @param screenId session identifier
  */
case class ReserveSeatRequest(imdbId: String, screenId: String)

/** Request to get info about movie session
  *
  * @param imdbId   IMDB identifier
  * @param screenId session identifier
  */
case class GetMovieInfoRequest(imdbId: String, screenId: String)

/** Response with movie session info
  *
  * @param imdbId         IMDB identifier
  * @param screenId       session identifier
  * @param totalSeats     total seats
  * @param availableSeats available seats
  * @param title          [[Option]] movie title or None
  */
case class GetMovieInfoResponse(imdbId: String, screenId: String, totalSeats: Int, availableSeats: Int, title: Option[String])

/** Internal request to retrieve movie title
  *
  * @param imdbId IMDB identifier
  */
case class RecognizeMovieTitleRequest(imdbId: String)

/** Response with found movie title
  *
  * @param imdbId IMDB identifier
  * @param title movie title or None
  */
case class RecognizeMovieTitleResponse(imdbId: String, title: Option[String])

/** Exception if movie session already registered
  *
  * @param imdbId IMDB id
  * @param screenId session id
  */
class SessionAlreadyExists(imdbId: String, screenId: String) extends Exception(s"Session with imdbId=$imdbId and screenId=$screenId already exists")

/** Exception if session is not registered
  *
  * @param imdbId IMDB id
  * @param screenId session id
  */
class SessionNotFound(imdbId: String, screenId: String) extends Exception(s"Session with imdbId=$imdbId and screenId=$screenId not found")

/** Exception if no seats available
  *
  * @param imdbId IMDB id
  * @param screenId session id
  */

class NoSeatsAvailable(imdbId: String, screenId: String) extends Exception(s"No seats available for session with imdbId=$imdbId and screenId=$screenId")

/** JSON serialization protocol
  */
trait ReservationProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  import spray.json._

  implicit val registerFormat: RootJsonFormat[RegisterMovieRequest] = jsonFormat3(RegisterMovieRequest.apply)

  implicit val reserveFormat: RootJsonFormat[ReserveSeatRequest] = jsonFormat2(ReserveSeatRequest.apply)

  implicit val getRequestFormat: RootJsonFormat[GetMovieInfoRequest] = jsonFormat2(GetMovieInfoRequest.apply)

  implicit val getResponseFormat: RootJsonFormat[GetMovieInfoResponse] = jsonFormat5(GetMovieInfoResponse.apply)

  implicit val errorFormat: RootJsonFormat[Error] = jsonFormat1(Error)

  implicit def errorResponseMarshaller: ToEntityMarshaller[Error] =
    Marshaller.withFixedContentType(MediaTypes.`application/json`) { error =>
      HttpEntity(error.toJson.compactPrint)
    }
}
