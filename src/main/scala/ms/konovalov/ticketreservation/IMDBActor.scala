package ms.konovalov.ticketreservation

import akka.actor.{Actor, ActorLogging, Props}
import akka.pattern.pipe
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

object IMDBActor {
  def prop(imdbClient: IMDBClient)(implicit mat: ActorMaterializer, ec: ExecutionContext): Props = Props(new IMDBActor(imdbClient))
}

/** Actor that processes requests to find movie title
  *
  * @param imdbClient client to external service
  * @param materializer implicit materializer
  * @param ec implicit execution context
  */
class IMDBActor(imdbClient: IMDBClient)(implicit materializer: ActorMaterializer, ec: ExecutionContext) extends Actor with ActorLogging {

  implicit private val system = this.context.system

  override def receive: Receive = {

    case RecognizeMovieTitleRequest(imdbId) =>
      pipe(requestIMDB(imdbId)) to sender()
  }

  private def requestIMDB(imdbId: String) = imdbClient.getData(imdbId).map(RecognizeMovieTitleResponse(imdbId, _))
}


