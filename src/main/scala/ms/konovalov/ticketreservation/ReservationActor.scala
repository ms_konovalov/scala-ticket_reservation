package ms.konovalov.ticketreservation

import akka.actor.Status.Status
import akka.actor.{Actor, ActorLogging, ActorRef, Props, Status}

object ReservationActor {
  def prop(imdbRecognizer: ActorRef): Props = Props(classOf[ReservationActor], imdbRecognizer)
}

/** Actor that stores movie info and provides all operations with data
  *
  * @param imdbActor actor responsible for taking data about movie title from external system
  */
class ReservationActor(private val imdbActor: ActorRef) extends Actor with ActorLogging {

  private var storage: Map[MovieSessionId, Seats] = Map.empty
  private var titles: Map[String, String] = Map.empty

  override def receive: Receive = {

    case RegisterMovieRequest(imdbId, screenId, seats) =>
      val sessionId = MovieSessionId(imdbId, screenId)
      val response: Status = if (storage.contains(sessionId)) {
        Status.Failure(new SessionAlreadyExists(imdbId, screenId))
      } else {
        storage += sessionId -> Seats(seats, seats)
        recognizeMovieTitle(imdbId)
        Status.Success()
      }
      sender() ! response

    case ReserveSeatRequest(imdbId, screenId) =>
      val sessionId = MovieSessionId(imdbId, screenId)
      val response: Status = if (!storage.contains(sessionId)) {
        Status.Failure(new SessionNotFound(imdbId, screenId))
      } else {
        val seats = storage(sessionId)
        if (seats.available > 0) {
          storage = storage.updated(sessionId, Seats(available = seats.available - 1, total = seats.total))
          Status.Success()
        } else {
          Status.Failure(new NoSeatsAvailable(imdbId, screenId))
        }
      }
      sender() ! response

    case GetMovieInfoRequest(imdbId, screenId) =>
      val sessionId = MovieSessionId(imdbId, screenId)
      sender() ! storage.get(sessionId).map(seats => GetMovieInfoResponse(imdbId, screenId, seats.total, seats.available, titles.get(imdbId)))
        .getOrElse(Status.Failure(new SessionNotFound(imdbId, screenId)))

    case RecognizeMovieTitleResponse(imdbId, title) =>
      title.foreach(titles += imdbId -> _)

  }

  private def recognizeMovieTitle(imdbId: String): Unit = {
    if (!titles.contains(imdbId)) {
      imdbActor ! RecognizeMovieTitleRequest(imdbId)
    }
  }
}
