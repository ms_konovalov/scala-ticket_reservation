package ms.konovalov.ticketreservation

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, ResponseEntity, StatusCodes}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.Materializer
import spray.json.{DefaultJsonProtocol, JsonFormat, RootJsonFormat}

import scala.concurrent.{ExecutionContext, Future}

/** Client to make requests to themoviedb.org
  *
  * @param apiKey api key to get access to service
  */
class IMDBClient(apiKey: Option[String]) extends ApiSerialization {

  private val url = (imdbId: String, apiKey: String) => s"https://api.themoviedb.org/3/find/$imdbId?api_key=$apiKey&external_source=imdb_id"

  /** Find title with imdbId
    *
    * @param imdbId IMDB identifier
    * @param mat implicit materializer
    * @param ec implicit execution context
    * @param system implicit actor system to make Http call and logging
    * @return future call with [[Option]] value of title or None
    */
  def getData(imdbId: String)(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[Option[String]] = {
    if (apiKey.isDefined) {
      val start = System.currentTimeMillis
      system.log.debug(s"call to imdb started")
      Http().singleRequest(HttpRequest(uri = url(imdbId, apiKey.get))).flatMap { response =>
        val time = System.currentTimeMillis - start
        system.log.debug(s"call to imdb finished for $time")
        deserialize[IMDBData](response).map(_.flatMap(_.movie_results.map(_.title).headOption))
      }
    } else {
      Future(None)
    }
  }

  private def deserialize[T](r: HttpResponse)(implicit um: Unmarshaller[ResponseEntity, T], mat: Materializer, ec: ExecutionContext): Future[Option[T]] =
    r.status match {
      case StatusCodes.OK => Unmarshal(r.entity).to[T] map Some.apply
      case _ => Future(None)
    }
}

/** Deserialization data from json
  */
trait ApiSerialization extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val itemFormat: JsonFormat[IMDBDataItem] = jsonFormat1(IMDBDataItem)

  implicit val dataFormat: RootJsonFormat[IMDBData] = jsonFormat1(IMDBData)

}

/** Representation of themoviedb.org data format
  *
  * @param movie_results array of results
  */
case class IMDBData(movie_results: Seq[IMDBDataItem])

/** Representation of themoviedb.org data format
  *
  * @param title movie title
  */
case class IMDBDataItem(title: String)