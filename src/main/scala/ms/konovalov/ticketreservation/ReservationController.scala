package ms.konovalov.ticketreservation

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.marshalling.ToResponseMarshallable.apply
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{HttpResponse, ResponseEntity, StatusCode}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.io.StdIn
import scala.language.postfixOps

/** HTTP Controller with API
  *
  * @param reservationActor ref on actor with business logic
  * @param am implicit materializer
  * @param ec implicit execution context
  * @param system implicit actor system for logging
  */
class ReservationController(reservationActor: ActorRef)
                           (implicit am: ActorMaterializer, ec: ExecutionContext, system: ActorSystem)
  extends ReservationProtocol {

  implicit val timeout = Timeout(5 seconds)

  val exceptionHandler = ExceptionHandler {
    case e: SessionAlreadyExists =>
      convertError(BadRequest, Error(e.getMessage))
    case e: SessionNotFound =>
      convertError(NotFound, Error(e.getMessage))
    case e: NoSeatsAvailable =>
      convertError(BadRequest, Error(e.getMessage))
    case e =>
      system.log.error(e, "Unexpected error")
      convertError(ServiceUnavailable, Error(s"Operation failed"))
  }

  val route: Route =
    handleExceptions(exceptionHandler) {
      pathPrefix("moviesession") {
        path("create") {
          post {
            entity(as[RegisterMovieRequest]) { data =>
              logRequest("POST-MOVIESESSION") {
                onComplete(reservationActor ? data) { result =>
                  complete(result.map(_ => HttpResponse(OK)).get)
                }
              }
            }
          }
        } ~
          path("get") {
            post {
              entity(as[GetMovieInfoRequest]) { data =>
                logRequest("POST-RETRIEVE") {
                  onComplete((reservationActor ? data).mapTo[GetMovieInfoResponse]) { result =>
                    complete(result.get)
                  }
                }
              }
            }
          } ~
          path("reserve") {
            post {
              entity(as[ReserveSeatRequest]) { data =>
                logRequest("POST-RESERVE") {
                  onComplete(reservationActor ? data) { result =>
                    complete(result.map(_ => HttpResponse(OK)).get)
                  }
                }
              }
            }
          }
      }
    }

  private def convertError(status: StatusCode, error: Error) = {
    onSuccess(Marshal(error).to[ResponseEntity]) { entity =>
      complete {
        HttpResponse(status = status, entity = entity)
      }
    }
  }
}

/** Class with error representation
  *
  * @param error message
  */
case class Error(error: String)

/** Main entry point
  */
object Main extends App {

  implicit val system = ActorSystem("my-system")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher

  val host = if (args.length > 0 && args(0) != null) args(0) else "0.0.0.0"
  val port = if (args.length > 1 && args(1) != null) args(1).toInt else 8080
  val apiKey = if (args.length > 2 && args(2) != null) Some(args(2)) else None
  val console = if (args.length > 3 && args(3) != null) args(3).toBoolean else false

  val imdbClient = new IMDBClient(apiKey)
  val imdbActor = system.actorOf(IMDBActor.prop(imdbClient))
  val reservationActor = system.actorOf(ReservationActor.prop(imdbActor))

  val bindingFuture = Http().bindAndHandle(new ReservationController(reservationActor).route, host, port)

  println(s"Movie ticket reservation server started on interface $host and port $port")
  sys.addShutdownHook(shutdown())

  if (console) {
    println("Press any key to exit...")
    StdIn.readLine()
    shutdown()
  }

  def shutdown(): Unit = {
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
  }
}