package ms.konovalov.ticketreservation

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationLong
import scala.language.postfixOps

class IMDBClientIT(_system: ActorSystem) extends TestKit(_system) with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec", ConfigFactory.parseString(
    """akka {
    |  loggers = ["akka.event.slf4j.Slf4jLogger"]
    |  loglevel = "DEBUG"
    |  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    }""".stripMargin)))

  implicit val materializer = ActorMaterializer()
  private val tmdbAppKey = sys.props.get("it.tmdb.appkey")
  private val client = new IMDBClient(tmdbAppKey)

  "request should be successful" in {
    val result = client.getData("tt0111161")
    Await.result(result, 1000 seconds) shouldEqual Some("The Shawshank Redemption")
  }

  "request should return None" in {
    val result = new IMDBClient(tmdbAppKey).getData("blabla")
    Await.result(result, 1000 seconds) shouldEqual None
  }

}
