package ms.konovalov.ticketreservation

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit}
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.{PatienceConfiguration, ScalaFutures}
import org.scalatest._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationDouble
import scala.language.postfixOps

object DockerTest extends Tag("dockerTest")

@WrapWith(classOf[ConfigMapWrapperSuite])
class ApiIT(configMap: Map[String, Any], _system: ActorSystem) extends TestKit(_system) with ImplicitSender with ReservationProtocol
  with WordSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach with MockFactory {

  implicit private val materializer = ActorMaterializer()
  implicit private val ec = ExecutionContext.Implicits.global
  implicit private val timeout: PatienceConfiguration.Timeout = Timeout(5 seconds)

  private val serviceKey = "ticket-api:8080"

  private def baseUrl: String = s"http://${configMap.getOrElse(serviceKey, getHostPortFormProps)}/moviesession"

  private def getHostPortFormProps: String = {
    val host = sys.props.get("it.host").getOrElse("localhost")
    val port = sys.props.get("it.port").getOrElse("8080").toInt
    s"$host:$port"
  }

  private def createUrl = s"$baseUrl/create"
  private def getUrl = s"$baseUrl/get"
  private def reserveUrl = s"$baseUrl/reserve"

  def this(configMap: Map[String, Any]) = this(configMap, ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  private def create[U](imdbId: String, screenId: String, seats: Int)(fun: HttpResponse => U) = {
    ScalaFutures.whenReady(Marshal(RegisterMovieRequest(imdbId, screenId, seats)).to[RequestEntity]) {
      call(createUrl, HttpMethods.POST, _)(fun)
    }
  }

  private def get[U](imdbId: String, screenId: String)(fun: HttpResponse => U) = {
    ScalaFutures.whenReady(Marshal(GetMovieInfoRequest(imdbId, screenId)).to[RequestEntity]) {
      call(getUrl, HttpMethods.POST, _)(fun)
    }
  }

  private def reserve[U](imdbId: String, screenId: String)(fun: HttpResponse => U) = {
    ScalaFutures.whenReady(Marshal(ReserveSeatRequest(imdbId, screenId)).to[RequestEntity]) {
      call(reserveUrl, HttpMethods.POST, _)(fun)
    }
  }

  private def call[U](url: String, method: HttpMethod, entity: RequestEntity)(fun: HttpResponse => U)(implicit timeout: PatienceConfiguration.Timeout) = {
    ScalaFutures.whenReady(
      Http().singleRequest(HttpRequest(uri = url, method = method, entity = entity)), timeout = timeout
    ) { response =>
      system.log.info(s"Calling ${method.value} for $url ends with ${response.status}")
      fun(response)
    }
  }

  "Client" must {

    val screenId1 = getRandom

    "receive NotFound when calling GET without adding movie session" taggedAs DockerTest in {
      get("aaa", screenId1) { response =>
        response.status shouldBe NotFound
      }
    }

    val screenId2 = getRandom

    "receive OK when creating and getting movie session" taggedAs DockerTest in {
      create("aaa", screenId2, 123) { response =>
        response.status shouldBe OK
        get("aaa", screenId2) { response =>
          response.status shouldBe OK
        }
      }
    }

    val screenId3 = getRandom

    "receive OK when creating and reservinr movie session" taggedAs DockerTest in {
      create("aaa", screenId3, 2) { response =>
        response.status shouldBe OK
        reserve("aaa", screenId3) { response =>
          response.status shouldBe OK
          reserve("aaa", screenId3) { response =>
            response.status shouldBe OK
            reserve("aaa", screenId3) { response =>
              response.status shouldBe BadRequest
            }
          }
        }
      }
    }
  }

  private def getRandom = UUID.randomUUID().toString
}
