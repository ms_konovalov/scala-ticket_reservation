package ms.konovalov.ticketreservation

import akka.actor.{ActorSystem, Status}
import akka.stream.ActorMaterializer
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.language.postfixOps

class ActorsIT(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  implicit val mat = ActorMaterializer()
  private val probe = TestProbe()
  private val tmdbAppKey = sys.props.get("it.tmdb.appkey")

  def this() = this(ActorSystem("MySpec", ConfigFactory.parseString(
    """akka {
    |  loggers = ["akka.event.slf4j.Slf4jLogger"]
    |  loglevel = "DEBUG"
    |  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    }""".stripMargin)))

  override def afterAll {
    TestKit.shutdownActorSystem(system, 20 seconds)
  }

  "ReservationActor actor" must {

    "successfully register movie session" in {
      val imdbClient = new IMDBClient(tmdbAppKey)
      val imdbActor = system.actorOf(IMDBActor.prop(imdbClient))
      val reservationActor = system.actorOf(ReservationActor.prop(imdbActor))

      probe.send(reservationActor, RegisterMovieRequest("tt0111161", "aaa", 15))
      probe.expectMsg(Status.Success())

      probe.send(reservationActor, ReserveSeatRequest("tt0111161", "aaa"))
      probe.expectMsg(Status.Success())

      probe.send(reservationActor, ReserveSeatRequest("tt0111161", "aaa"))
      probe.expectMsg(Status.Success())

      probe.send(reservationActor, ReserveSeatRequest("tt0111161", "aaa"))
      probe.expectMsg(Status.Success())

      probe.send(reservationActor, GetMovieInfoRequest("tt0111161", "aaa"))
      probe.expectMsg(GetMovieInfoResponse("tt0111161", "aaa", 15, 12, None))

      retry(10, 1 seconds) {
        probe.send(reservationActor, GetMovieInfoRequest("tt0111161", "aaa"))
        probe.expectMsg(GetMovieInfoResponse("tt0111161", "aaa", 15, 12, Some("The Shawshank Redemption")))
      }

    }
  }

  def retry[T](times: Int, delay: FiniteDuration)(fn: => T): Option[T] =
    (1 to times).view flatMap (n => try Some(fn)
    catch {
      case _: Throwable =>
        Thread.sleep(delay.toMillis)
        None
    }) headOption
}
