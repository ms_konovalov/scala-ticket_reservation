package ms.konovalov.ticketreservation

import akka.actor.{ActorSystem, Status}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

class ReservationActorTest(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  val probe = TestProbe()

  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "ReservationActor actor" must {

    "successfully register movie session" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      probe.expectMsg(RecognizeMovieTitleRequest("imdb"))
      reservationActor ! RegisterMovieRequest("imdb2", "screen", 100)
      expectMsg(Status.Success())
      probe.expectMsg(RecognizeMovieTitleRequest("imdb2"))
      reservationActor ! RegisterMovieRequest("imdb3", "screen", 300)
      expectMsg(Status.Success())
      probe.expectMsg(RecognizeMovieTitleRequest("imdb3"))
    }

    "fail to register same movie session" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! RegisterMovieRequest("imdb", "screen", 200)
      expectMsgClass(classOf[Status.Failure])
      reservationActor ! RegisterMovieRequest("imdb", "screen", 300)
      expectMsgClass(classOf[Status.Failure])
    }

    "successfully reserve seats for existing movie " in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsg(Status.Success())
    }

    "fail to reserve seats for not-existing movie" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb1", "screen")
      expectMsgClass(classOf[Status.Failure])
      reservationActor ! ReserveSeatRequest("imdb", "screen1")
      expectMsgClass(classOf[Status.Failure])
    }

    "fail to reserve seats over available" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 2)
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsgClass(classOf[Status.Failure])
      reservationActor ! ReserveSeatRequest("imdb", "screen")
      expectMsgClass(classOf[Status.Failure])
    }

    "successfully respond about existing movie" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! RegisterMovieRequest("imdb2", "screen2", 200)
      expectMsg(Status.Success())
      reservationActor ! GetMovieInfoRequest("imdb", "screen")
      expectMsg(GetMovieInfoResponse("imdb", "screen", 100, 100, None))
      reservationActor ! ReserveSeatRequest("imdb2", "screen2")
      expectMsg(Status.Success())
      reservationActor ! ReserveSeatRequest("imdb2", "screen2")
      expectMsg(Status.Success())
      reservationActor ! GetMovieInfoRequest("imdb2", "screen2")
      expectMsg(GetMovieInfoResponse("imdb2", "screen2", 200, 198, None))
    }

    "fail to respond about non-existing movie" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! GetMovieInfoRequest("imdb2", "screen2")
      expectMsgClass(classOf[Status.Failure])
    }

    "respond with title about existing movie" in {
      val reservationActor = system.actorOf(ReservationActor.prop(probe.ref))
      reservationActor ! RegisterMovieRequest("imdb", "screen", 100)
      expectMsg(Status.Success())
      reservationActor ! GetMovieInfoRequest("imdb", "screen")
      expectMsg(GetMovieInfoResponse("imdb", "screen", 100, 100, None))
      probe.send(reservationActor, RecognizeMovieTitleResponse("imdb", Some("title")))
      reservationActor ! GetMovieInfoRequest("imdb", "screen")
      expectMsg(GetMovieInfoResponse("imdb", "screen", 100, 100, Some("title")))
      probe.send(reservationActor, RecognizeMovieTitleResponse("imdb", Some("title2")))
      reservationActor ! GetMovieInfoRequest("imdb", "screen")
      expectMsg(GetMovieInfoResponse("imdb", "screen", 100, 100, Some("title2")))
    }
  }
}
