package ms.konovalov.ticketreservation

import akka.actor.{Actor, Status}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ContentType, HttpEntity, MediaTypes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestActorRef, TestProbe}
import org.scalamock.scalatest.MockFactory
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

class ReservationControllerTest extends WordSpec with Matchers with ScalatestRouteTest with ReservationProtocol
  with MockFactory with TableDrivenPropertyChecks {

  private val probe = TestProbe().ref
  private val reservationRoute = new ReservationController(probe).route

  "The service" should {

    "return a NotFound error for GET requests to the root path" in {
      Get() ~> Route.seal(reservationRoute) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a NotFound error for POST requests to the /moviesession" in {
      Post("/moviesession") ~> Route.seal(reservationRoute) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a OK for POST requests to the /moviesession/create" in {
      val route: Route = createRoute(_ => Status.Success())
      Post("/moviesession/create", RegisterMovieRequest("1", "2", 100)) ~> Route.seal(route) ~> check {
        status shouldEqual OK
      }
    }

    "return error for POST requests to the /moviesession/create if session already exists" in {
      val route: Route = createRoute(_ => Status.Failure(new SessionAlreadyExists("1", "2")))
      Post("/moviesession/create", RegisterMovieRequest("1", "2", 100)) ~> Route.seal(route) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return error for POST requests to the /moviesession/create if error" in {
      val route: Route = createRoute(_ => Status.Failure(new RuntimeException("")))
      Post("/moviesession/create", RegisterMovieRequest("1", "2", 100)) ~> Route.seal(route) ~> check {
        status shouldEqual ServiceUnavailable
      }
    }

    "return error for POST requests to the /moviesession/create with incorrect request" in {
      val entity = HttpEntity(ContentType(MediaTypes.`application/json`), """{"imdbId" : "data", "screenId" : "some", "availableSeats" : "aaa"}""")
      Post("/moviesession/create", entity) ~> Route.seal(reservationRoute) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return a OK for POST requests to the /moviesession/get" in {
      val route: Route = createRoute({case x: GetMovieInfoRequest => GetMovieInfoResponse(x.imdbId, x.screenId, 100, 50, None)})
      Post("/moviesession/get", GetMovieInfoRequest("1", "2")) ~> Route.seal(route) ~> check {
        status shouldEqual OK
        entityAs[GetMovieInfoResponse] shouldEqual GetMovieInfoResponse("1", "2", 100, 50, None)
      }
    }

    "return error for POST requests to the /moviesession/get if session not found" in {
      val route: Route = createRoute(_ => Status.Failure(new SessionNotFound("1", "2")))
      Post("/moviesession/create", RegisterMovieRequest("1", "2", 100)) ~> Route.seal(route) ~> check {
        status shouldEqual NotFound
      }
    }

    "return error for POST requests to the /moviesession/get if error" in {
      val route: Route = createRoute(_ => Status.Failure(new RuntimeException("")))
      Post("/moviesession/create", RegisterMovieRequest("1", "2", 100)) ~> Route.seal(route) ~> check {
        status shouldEqual ServiceUnavailable
      }
    }

    "return error for POST requests to the /moviesession/get with incorrect request" in {
      val entity = HttpEntity(ContentType(MediaTypes.`application/json`), """{"imdbId" : "data", "screenId" : 123}""")
      Post("/moviesession/create", entity) ~> Route.seal(reservationRoute) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return a OK for POST requests to the /moviesession/reserve" in {
      val route: Route = createRoute(_ => Status.Success())
      Post("/moviesession/reserve", ReserveSeatRequest("1", "2")) ~> Route.seal(route) ~> check {
        status shouldEqual OK
      }
    }

    "return a error for POST requests to the /moviesession/reserve if session not found" in {
      val route: Route = createRoute({case ReserveSeatRequest(x, y) => Status.Failure(new SessionNotFound(x, y))})
      Post("/moviesession/reserve", ReserveSeatRequest("1", "2")) ~> Route.seal(route) ~> check {
        status shouldEqual NotFound
      }
    }

    "return a error for POST requests to the /moviesession/reserve if no seats available" in {
      val route: Route = createRoute({case ReserveSeatRequest(x, y) => Status.Failure(new NoSeatsAvailable(x, y))})
      Post("/moviesession/reserve", ReserveSeatRequest("1", "2")) ~> Route.seal(route) ~> check {
        status shouldEqual BadRequest
      }
    }

    "return error for POST requests to the /moviesession/reserve with incorrect request" in {
      val entity = HttpEntity(ContentType(MediaTypes.`application/json`), """{"imdbId" : "data", "availableSeats" : "aaa"}""")
      Post("/moviesession/reserve", entity) ~> Route.seal(reservationRoute) ~> check {
        status shouldEqual BadRequest
      }
    }
  }

  private def createRoute(pf: Any => Any) =
    new ReservationController(TestActorRef(new Actor {
      def receive: PartialFunction[Any, Unit] = {
        case x => sender() ! pf.apply(x)
      }
    })).route
}