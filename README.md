Movie ticket reservation service 
================================

This app is based on scala, akka and akka-http. 
It provides ability to register movie session in system, book seats and retrieve info about registered movie sessions and available seats.

### API ###

The following API is available:  
1) POST data to register movie session on  

``http://{host}:{port}/moviesession/create`` 

    {
        "imdbId": "tt0111161",
        "availableSeats": 100,
        "screenId": "screen_123456"
    }

If movie session is stored successfully you will receive ``OK`` response without body.  
In case of error you will get common error response with error description

``Status Code: 400 Bad Request``   
``Content-Type: application/json``

    {
      "error" : "Session with imdbId=tt0111161 and screenId=1 already exists"
    }

2) POST request to reserve 1 seat for provided movie session  

``http://{host}:{port}/moviesession/reserve``

    {
        "imdbId": "tt0111161",
        "screenId": "screen_123456"
    }
        
If provided movie session is not registered in app you will get error response

``Status Code: 404 Not Found``   
``Content-Type: application/json``

    {
      "error" : "Session with imdbId=tt0111161 and screenId=21 not found"
    }
        
3) POST request to retrieve info about movie session

``http://{host}:{port}/moviesession/get``

    {
        "imdbId": "tt0111161",
        "screenId": "screen_123456"
    }
        
If movie session exists you will get the following response

``Status Code: 200 OK``   
``Content-Type: application/json``

    {
        "imdbId": "tt0111161",
        "screenId": "screen_123456",
        "movieTitle": "The Shawshank Redemption",
        "availableSeats": 100,
        "reservedSeats": 50
    }

### Integration with IMDB Data ###

To provide data about movie titles app is integrated with external service https://www.themoviedb.org/ which provides REST API 
to movie Database and ability to find movie title by imdbId. 
Requests to this exteranal system is being made asynchronously in background - so if you get data just after registering movie session 
you will receive data without title (as request to external system is not finished yet). But if you repeat your request after several seconds 
(and provided imdbId is valid) you will see movie title in response.

To start using this integration you need to be registered on https://www.themoviedb.org/ and generate api key and set it to the app.


### Starting ###

To compile and start application go to root directory and 
 
    % sbt package
    
    [info] Loading project definition from ./ticket-reservation/project
    [info] Updating {file:./ticket-reservation/project/}ticket-reservation-build...
    [info] Resolving org.fusesource.jansi#jansi;1.4 ...
    [info] Done updating.
    [info] Set current project to ticket-reservation (in build file:./ticket-reservation/)
    [info] Updating {file:./ticket-reservation/}root...
    [info] Resolving jline#jline;2.14.3 ...
    [info] Done updating.
    [info] Compiling 1 Scala source to ./ticket-reservation/target/scala-2.12/classes...
    [info] Packaging ./ticket-reservation/target/scala-2.12/ticket-reservation_2.12-1.0.jar ...
    [info] Done packaging.
    [success] Total time: 10 s, completed May 29, 2017 9:44:19 AM

    
    % sbt "run 127.0.0.1 8080 <tmdb_api_key>"
    
    [info] Loading project definition from ./ticket-reservation/project
    [info] Set current project to ticket-reservation (in build file:./projects/ticket-reservation/)
    [info] Running ms.konovalov.ticketreservation.Main 127.0.0.1 8080 <tmdb_api_key>
    Movie ticket reservation server started on interface 127.0.0.1 and port 8080
    Press any key to exit...


### Integration testing ###

Project contains several integration tests which can be started manually with ScalaTest or via SBT

``ms.konovalov.ticketreservation.ActorsIT`` and ``ms.konovalov.ticketreservation.IMDBClientIT`` uses https://www.themoviedb.org/ 
API so corresponding API key has to be provided during startup  

    % sbt it:test -Dit.tmdb.appkey=<tmdb_api_key>

``ms.konovalov.ticketreservation.ApiIT`` has to be run against started application so additionally host and port has to be set up.
It can be done with:
  
    -Dit.host=<host> -Dit.port=<port>
  
Also it is possible to run integration test against application run in docker container (_docker_ and _docker-compose_ must be installed on your pc).

    % sbt -Dit.tmdb.appkey=<tmdb_api_key> dockerComposeTest