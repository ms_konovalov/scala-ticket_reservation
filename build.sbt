import java.io.File

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    scalaVersion := "2.12.2",
    name := """ticket-reservation""",
    version := "1.0",
    Defaults.itSettings,
    libraryDependencies ++= Dependencies.main,
    parallelExecution in Test := false
  ).enablePlugins(DockerPlugin, DockerComposePlugin)

val mainClassStr = "ms.konovalov.ticketreservation.Main"
mainClass in (Compile, run) := Some(mainClassStr)

val dockerAppPath: String = "/app/"

val apiKey = Option(System.getProperty("it.tmdb.appkey")).getOrElse("key")

dockerfile in docker := {
  new Dockerfile {
    val classpath: Classpath = (fullClasspath in Compile).value
    from("java")
    add(classpath.files, dockerAppPath)
    entryPoint("java", "-cp", s"$dockerAppPath:$dockerAppPath*", s"$mainClassStr", "0.0.0.0", "8080", apiKey, "false")
  }
}

imageNames in docker := Seq(
  ImageName(s"${organization.value}/${name.value}:latest"),
  ImageName(s"${organization.value}/${name.value}:v${version.value}")
)

variablesForSubstitution := Map("ORGID" -> organization.value, "PROJID" -> name.value)

dockerImageCreationTask := docker.value

testCasesPackageTask := (sbt.Keys.packageBin in IntegrationTest).value
testCasesJar := artifactPath.in(IntegrationTest, packageBin).value.getAbsolutePath
testDependenciesClasspath := {
  val fullClasspathCompile = (fullClasspath in Compile).value
  val classpathTestManaged = (managedClasspath in IntegrationTest).value
  val classpathTestUnmanaged = (unmanagedClasspath in IntegrationTest).value
  val testResources = (resources in IntegrationTest).value
  (fullClasspathCompile.files ++ classpathTestManaged.files ++ classpathTestUnmanaged.files ++ testResources).map(_.getAbsoluteFile).mkString(File.pathSeparator)
}
testExecutionExtraConfigTask := Map("it.tmdb.appkey" -> apiKey)
